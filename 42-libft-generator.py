# ******************************************************************************************************************** #
#                                                                                                                      #
#                                                                                                 :::      ::::::::    #
#    42-libft-generator.py                                                                      :+:      :+:    :+:    #
#                                                                                             +:+ +:+         +:+      #
#    By: sessaidi <sessaidi@student.42.fr>                                                  +#+  +:+       +#+         #
#                                                                                         +#+#+#+#+#+   +#+            #
#    Created: 2022/06/14 17:32:04 by sessaidi                                                  #+#    #+#              #
#    Updated: 2022/06/14 17:58:28 by sessaidi                                                 ###   ########.fr        #
#                                                                                                                      #
# ******************************************************************************************************************** #

import typing
import argparse
import os
import ctypes
import random
from enum import Enum
import linecache as lc
from datetime import datetime
from string import Template

g_dir_exclude: list[str] = []
g_dir_project: str = ""
g_verbose: bool = False
g_files: list[str] = []

class Message(Enum):
	FILES_NOT_FOUND = "No files were found. Check if you are on the right directory.",
	MANY_EXTENSION = "Multiple Language still not supported. Be sure to have same source extension",
	VERBOSE = "Enable verbose",
	PROJECT_PATH = "Set project path, by default '.'",
	EXCLUDE_PATH = "Folder to exclude"

def is_hidden(filepath):
	name = os.path.basename(os.path.abspath(filepath))
	return name.startswith('.') or has_hidden_attribute(filepath)

def has_hidden_attribute(filepath):
	try:
		attrs = ctypes.windll.kernel32.GetFileAttributesW(unicode(filepath))
		assert attrs != -1
		result = bool(attrs & 2)
	except (AttributeError, AssertionError):
		result = False
	return result

def scan_project() -> list[str]:
	result: list[str] = []
	for path, subdirs, files in os.walk("."):
		subdirs[:] = [d for d in subdirs if d not in g_dir_exclude]
		for name in files:
			result.append(os.path.join(path, name))
	return result

def get_options() -> None:
	global g_verbose, g_dir_project, g_dir_exclude, g_authors

	parser = argparse.ArgumentParser()
	parser.add_argument("-v", "--verbose", help=Message.VERBOSE, action="store_true")
	parser.add_argument("-p", "--project", help=Message.PROJECT_PATH, default=os.getcwd())
	parser.add_argument("-e", "--exclude", help=Message.EXCLUDE_PATH, action="append")
	args = parser.parse_args()
	g_verbose = args.verbose
	g_dir_project = args.project
	g_dir_exclude.append(args.exclude)

def is_header(filepath: str) -> bool:
	if is_hidden(filepath):
		return False
	if filepath.endswith(tuple([
			".h", 
			".hh", 
			".hpp", 
		])):
		return True
	return False

g_template: str = "#ifndef LIBFT_H\n# define LIBFT_H\n\n$headers\n\n#endif"

def write_libft(headers: list[str]) -> None:
	headers = list(map(lambda h: "# include \"" + h + "\"", headers))
	data = dict(headers = "\n".join(headers))
	src = Template(g_template)
	result = src.safe_substitute(data)
	with open("libft.h", "w") as f:
		f.write(result)
	print("libft header generated in " + g_dir_project)



def main():
	global	g_files

	get_options()
	os.chdir(g_dir_project)
	files: list[str] = scan_project()
	g_files = list(filter(is_header, files))

	if g_files:
		headers: list[str] = []
		for file in g_files:
			filename: str = os.path.basename(os.path.abspath(file))
			headers.append(filename)
		write_libft(headers)

if __name__ == "__main__":
	main()

