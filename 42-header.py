import typing
import argparse
import os
import ctypes
import random
from enum import Enum
import linecache as lc
from datetime import datetime

g_dir_exclude: list[str] = []
g_dir_project: str = ""
g_verbose: bool = False
g_files: list[str] = []
g_authors: list[str] = []

g_asciiart: list[str] = [
	"        :::      ::::::::",
	"      :+:      :+:    :+:",
	"    +:+ +:+         +:+  ",
	"  +#+  +:+       +#+     ",
	"+#+#+#+#+#+   +#+        ",
	"     #+#    #+#          ",
	"    ###   ########.fr    "
]

class Message(Enum):
	FILES_NOT_FOUND = "No files were found. Check if you are on the right directory.",
	MANY_EXTENSION = "Multiple Language still not supported. Be sure to have same source extension",
	VERBOSE = "Enable verbose",
	PROJECT_PATH = "Set project path, by default '.'",
	EXCLUDE_PATH = "Folder to exclude"
	AUTHOR = "Author of the project"

def is_allowed(filepath: str) -> bool:
	if is_hidden(filepath):
		return False
	if filepath.endswith(tuple([
			".c", 
			".cc", 
			".cpp", 
			".cxx", 
			".h", 
			".hh", 
			".hpp", 
			".hxx", 
			".asm", 
			".s", 
			".go", 
			".f", 
			".f90", 
			".f95", 
			".f03", 
			".f15", 
			".for", 
			".m", 
			".mm",
			".swift",
			".js",
			".ts",
			".css",
			".scss",
			".sass",
			".kt",
			".kts",
			".rs",
			".py",
			".html",
			".xml",
			".vim",
			".java",
			".pl",
			".rb",
			".tex",
			".lua",
			".sql",
		])):
		return True
	return False

def is_not_test(filepath: str) -> bool:
	if is_hidden(filepath):
		return False
	if filepath.endswith(tuple([
			"_test.c", 
			"_test.cc", 
			"_test.cpp", 
			"_test.cxx", 
			"_test.asm", 
			"_test.s", 
			"_test.go", 
			"_test.f", 
			"_test.f90", 
			"_test.f95", 
			"_test.f03", 
			"_test.f15", 
			"_test.for", 
			"_test.m", 
			"_test.mm",
			"_test.swift",
			".test.c", 
			".test.cc", 
			".test.cpp", 
			".test.cxx", 
			".test.asm", 
			".test.s", 
			".test.go", 
			".test.f", 
			".test.f90", 
			".test.f95", 
			".test.f03", 
			".test.f15", 
			".test.for", 
			".test.m", 
			".test.mm",
			".test.swift",
			"_spec.c", 
			"_spec.cc", 
			"_spec.cpp", 
			"_spec.cxx", 
			"_spec.asm", 
			"_spec.s", 
			"_spec.go", 
			"_spec.f", 
			"_spec.f90", 
			"_spec.f95", 
			"_spec.f03", 
			"_spec.f15", 
			"_spec.for", 
			"_spec.m", 
			"_spec.mm",
			"_spec.swift"
			".spec.c", 
			".spec.cc", 
			".spec.cpp", 
			".spec.cxx", 
			".spec.asm", 
			".spec.s", 
			".spec.go", 
			".spec.f", 
			".spec.f90", 
			".spec.f95", 
			".spec.f03", 
			".spec.f15", 
			".spec.for", 
			".spec.m", 
			".spec.mm",
			".spec.swift"
		])):
		return False
	return True

def is_hidden(filepath):
	name = os.path.basename(os.path.abspath(filepath))
	return name.startswith('.') or has_hidden_attribute(filepath)

def has_hidden_attribute(filepath):
	try:
		attrs = ctypes.windll.kernel32.GetFileAttributesW(unicode(filepath))
		assert attrs != -1
		result = bool(attrs & 2)
	except (AttributeError, AssertionError):
		result = False
	return result

def scan_project() -> list[str]:
	result: list[str] = []
	for path, subdirs, files in os.walk("."):
		subdirs[:] = [d for d in subdirs if d not in g_dir_exclude]
		for name in files:
			result.append(os.path.join(path, name))
	return result

def get_options() -> None:
	global g_verbose, g_dir_project, g_dir_exclude, g_authors

	parser = argparse.ArgumentParser()
	parser.add_argument("-v", "--verbose", help=Message.VERBOSE, action="store_true")
	parser.add_argument("-p", "--project", help=Message.PROJECT_PATH, default=".")
	parser.add_argument("-e", "--exclude", help=Message.EXCLUDE_PATH, action="append")
	parser.add_argument("-a", "--author", help=Message.AUTHOR, action="append", required=True)
	args = parser.parse_args()
	g_verbose = args.verbose
	g_dir_project = args.project
	g_dir_exclude.append(args.exclude)
	for author in args.author:
		if author not in g_authors	and len(author.strip()) > 0:
			g_authors.append(author)

def get_filetype(filepath: str) -> dict:
	info: dict = dict({"start": "#", "end": "#", "fill": "*", "length": 120, "margin": 5})
	filename, extension = os.path.splitext(filepath)
	if extension in [
			".c",
			".h",
			".cc",
			".cpp",
			".cxx",
			".hpp",
			".hp",
			".hx",
			".js",
			".ts",
			".go",
			".m",
			".mm",
			".rs",
			".kt",
			".kts",
			".sql",
			".swift",
			".css",
			".scss",
			".java"
			]:
		info["start"] = "/*"
		info["end"] = "*/"
	if extension in [".c", ".h", ".s", ".asm", ".ml", ".mli", ".mll", ".mly", ".f", ".f90", ".f95", ".f03", ".for"]:
		info["length"] = 80
	if extension in [ ".f", ".f90", ".f95", ".f03",".for"]:
		info["start"] = info["end"] = "!"
		info["fill"] = "/"
	if extension in [ ".ml", ".mli", ".mll", ".mly"]:
		info["start"] = "(*"
		info["end"] = "*)"
	if extension in [ ".html", ".xml"]:
		info["start"] = "<!--"
		info["end"] = "-->"
	if extension in [ ".vim", ".lua"]:
		info["start"] = ("\"", "--[[")[extension == ".lua"]
		info["end"] = ("\"", "]]--")[extension == ".lua"]
		info["length"] = 100
	if extension in [".tex"]:
		info["start"] = info["end"] =  "%"
	return info

def reserve_area(filepath: str, info: list[str]) -> None:
	with open(filepath, "r") as f:
		content: list[str] = f.readlines()
		area: list[str] = []
	with open(filepath, "w") as f:
		f.seek(0, 0)

		lstart: int = len(info["start"].ljust(info["margin"]))
		lend: int = len(info["end"].ljust(info["margin"]))

		for i in list(range(11)):
			if i in [0, 10]:
				area.append(info["start"] + " ".ljust(info["length"] - (len(info["start"]) + len(info["end"]) + 1), '*') + " " + info["end"] + "\n")
			else:
				area.append(info["start"].ljust(info["length"] - lend) + info["end"].rjust(info["margin"]) + "\n")

		for pos, line in enumerate(g_asciiart):
			area[pos + 2] = info["start"].ljust(info["length"] - lend - len(line.ljust(info["margin"] - len(info["end"])))) + line.ljust(info["margin"] - len(info["end"])) + info["end"].rjust(info["margin"]) + "\n"

		area.append("\n")
		area += content
		f.writelines(area)

def set_filename(filepath: str, info: list[str]) -> None:
	with open(filepath, "r") as f:
		content: str = f.readlines()
		filename: str = os.path.basename(filepath)
		lstart: int = len(info["start"].ljust(info["margin"]))
		lend: int = len(info["end"].rjust(info["margin"]))
		lascii: int = len(g_asciiart[1].ljust(info["margin"]))
		line: str = info["start"].ljust(info["margin"])
		line += filename.ljust(info["length"] - (lstart + lend + lascii))
		line += g_asciiart[1].ljust(info["margin"] - len(info["end"]))
		line += info["end"].rjust(info["margin"]) + "\n"
		content[3] = line
	with open(filepath, "w") as f:
		f.writelines(content)

def roulette(filepath: str) -> str:
	print("All right Folks!!")
	print("Step Right Up !!")
	print("Step Right Up !!")
	print("Place your bets !")
	print("Here we are")
	filename: str = os.path.basename(filepath)
	author: str = g_authors[random.randint(0, len(g_authors) - 1)]
	print("All right folks, for " + filename + " the author will be " + author)
	return author

def set_author(filepath: str, info: list[str], author: str) -> None:
	with open(filepath, "r") as f:
		content: str = f.readlines()

		lstart: int = len(info["start"].ljust(info["margin"]))
		lend: int = len(info["end"].ljust(info["margin"]))
		lascii: int = len(g_asciiart[3].ljust(info["margin"]))

		line: str = info["start"].ljust(info["margin"])
		line += "By: " + author + " <" + author
		line += "@student.42.fr>".ljust(info["length"] -(len(line) + lend + lascii))
		line += g_asciiart[3].ljust(info["margin"] - len(info["end"]))
		line += info["end"].rjust(info["margin"]) + "\n"
		content[5] = line
	with open(filepath, "w") as f:
		f.writelines(content)

def set_date_creation(filepath: str, info: list[str], author: str) -> None:
	with open(filepath, "r") as f:
		content: str = f.readlines()

		stat = os.stat(filepath)
		ts = int(stat.st_ctime)
		dt = datetime.utcfromtimestamp(ts).strftime('%Y/%m/%d %H:%M:%S')

		lstart: int = len(info["start"].ljust(info["margin"]))
		lend: int = len(info["end"].ljust(info["margin"]))
		lascii: int = len(g_asciiart[5].ljust(info["margin"]))

		line: str = info["start"].ljust(info["margin"])
		line += "Created: " + dt + " by "
		line += author.ljust(info["length"] -(len(line) + lend + lascii))
		line += g_asciiart[5].ljust(info["margin"] - len(info["end"]))

		line += info["end"].rjust(info["margin"]) + "\n"

		content[7] = line

	with open(filepath, "w") as f:
		f.writelines(content)

def set_date_update(filepath: str, info: list[str], author: str) -> None:
	with open(filepath, "r") as f:
		content: str = f.readlines()

		ts = int(os.path.getmtime(filepath))
		dt = datetime.utcfromtimestamp(ts).strftime('%Y/%m/%d %H:%M:%S')

		lstart: int = len(info["start"].ljust(info["margin"]))
		lend: int = len(info["end"].ljust(info["margin"]))
		lascii: int = len(g_asciiart[6].ljust(info["margin"]))

		line: str = info["start"].ljust(info["margin"])
		line += "Updated: " + dt + " by "
		line += author.ljust(info["length"] -(len(line) + lend + lascii))
		line += g_asciiart[6].ljust(info["margin"] - len(info["end"]))

		line += info["end"].rjust(info["margin"]) + "\n"

		content[8] = line

	with open(filepath, "w") as f:
		f.writelines(content)


def insert(filepath: str, info: list[str], author: str) -> None:
	reserve_area(filepath, info)
	set_filename(filepath, info)
	set_author(filepath, info, author)
	set_date_creation(filepath, info, author)
	set_date_update(filepath, info,author)

def update(filepath: str) -> bool:
	info: dict = get_filetype(filepath)
	line_1: str = lc.getline(filepath, 1)
	line_2: str = lc.getline(filepath, 2)
	line_6: str = lc.getline(filepath, 6)
	line_8: str = lc.getline(filepath, 8)
	line_9: str = lc.getline(filepath, 9)
	line_10: str = lc.getline(filepath, 10)
	line_11: str = lc.getline(filepath, 11)

	control_1: str = info["start"] + " ".ljust(info["length"] - (len(info["start"]) + len(info["end"]) + 1), '*') + " " + info["end"]
	control_2: str = info["start"].ljust(info["length"] - len(info["end"])) + info["end"]
	control_3: str = info["start"].ljust(info["margin"]) + "By: "
	control_4: str = info["start"].ljust(info["margin"]) + "Created: "
	control_5: str = info["start"].ljust(info["margin"]) + "Updated: "
	control_6: str = info["start"].ljust(info["length"] - len(info["end"]), ' ') + info["end"]
	control_7: str = info["start"] + " ".ljust(info["length"] - (len(info["start"]) + len(info["end"]) + 1), '*') + " " + info["end"]

	if line_1.startswith(control_1):
		if line_2.startswith(control_2):
			if line_6.startswith(control_3):
				if line_8.startswith(control_4):
					if line_9.startswith(control_5):
						if line_10.startswith(control_6):
							if line_11.startswith(control_7):
								return True
	return False

def main():
	global	g_files_source

	get_options()
	os.chdir(g_dir_project)
	files: list[str] = scan_project()
	g_files = list(filter(is_allowed, files))
	for file in g_files:
		info: dict = get_filetype(file)
		author: str = roulette(file)
		if not update(file):
			insert(file, info, author)
		else:
			set_date_update(file, info, author)

if __name__ == "__main__":
	main()
