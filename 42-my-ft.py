import typing
import argparse
import os
import ctypes
import random
from enum import Enum
import linecache as lc
from datetime import datetime
import os, fnmatch

g_dir_exclude: list[str] = []
g_dir_project: str = ""
g_verbose: bool = False
g_files: list[str] = []

class Message(Enum):
	VERBOSE = "Enable verbose",
	PROJECT_PATH = "Set project path, by default '.'",
	EXCLUDE_PATH = "Folder to exclude"

def is_allowed(filepath: str) -> bool:
	if is_hidden(filepath):
		return False
	if filepath.endswith(tuple([
			".c", 
			".cc", 
			".cpp", 
			".cxx", 
			".h", 
			".hh", 
			".hpp", 
			".hxx", 
			".asm", 
			".s", 
			".go", 
			".f", 
			".f90", 
			".f95", 
			".f03", 
			".f15", 
			".for", 
			".m", 
			".mm",
			".swift",
			".js",
			".ts",
			".css",
			".scss",
			".sass",
			".kt",
			".kts",
			".rs",
			".py",
			".html",
			".xml",
			".vim",
			".java",
			".pl",
			".rb",
			".tex",
			".lua",
			".sql",
		])):
		return True
	return False

def is_hidden(filepath):
	name = os.path.basename(os.path.abspath(filepath))
	return name.startswith('.') or has_hidden_attribute(filepath)

def has_hidden_attribute(filepath):
	try:
		attrs = ctypes.windll.kernel32.GetFileAttributesW(unicode(filepath))
		assert attrs != -1
		result = bool(attrs & 2)
	except (AttributeError, AssertionError):
		result = False
	return result

def scan_project() -> list[str]:
	result: list[str] = []
	for path, subdirs, files in os.walk("."):
		subdirs[:] = [d for d in subdirs if d not in g_dir_exclude]
		for name in files:
			result.append(os.path.join(path, name))
	return result

def get_options() -> None:
	global g_verbose, g_dir_project, g_dir_exclude, g_authors

	parser = argparse.ArgumentParser()
	parser.add_argument("-v", "--verbose", help=Message.VERBOSE, action="store_true")
	parser.add_argument("-p", "--project", help=Message.PROJECT_PATH, default=".")
	parser.add_argument("-e", "--exclude", help=Message.EXCLUDE_PATH, action="append")
	args = parser.parse_args()
	g_verbose = args.verbose
	g_dir_project = args.project
	g_dir_exclude.append(args.exclude)

def update_content(filepath: str) -> None:
	with open(filepath) as f:
		s = f.read()
		s = s.replace("my_", "ft_")
	with open(filepath, "w") as f:
		f.write(s)

def update_filename(filepath: str) -> None:
	os.rename(filepath, (filepath.replace("my_", "ft_")))


def main():
	global	g_files_source

	get_options()
	os.chdir(g_dir_project)
	files: list[str] = scan_project()
	g_files = list(filter(is_allowed, files))
	for file in g_files:
		update_content(file)
		update_filename(file)

if __name__ == "__main__":
	main()
